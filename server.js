// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');
var crypto = require('crypto');


// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});

app.post("/login", function(req, res, next) {

    var login = req.body.login;
    var password = req.body.password;
    var token = crypto.randomBytes(64).toString('hex');
    console.log(token);

    db.all("SELECT * FROM users WHERE ident=? and password=? ;",[login, password], function (err, data) {
        if (data.length === 0) {
            res.json({status : false})
        }
        else {
            db.all("SELECT * FROM sessions WHERE ident=? and token=?;",[login,token], function (err, data) {
                if (data.length === 0){
                    db.all("INSERT INTO sessions (ident, token) VALUES (?, ?);", [login, token]);
                    res.json({status: true, token : token});
                }
            });
        }
    });
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
